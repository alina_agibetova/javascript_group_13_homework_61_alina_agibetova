import { Component, Input, OnInit } from '@angular/core';
import { Film } from '../shared/film.model';
import { FilmService } from '../shared/film.service';
import { AboutFilm } from '../shared/aboutFilm.model';
import { AboutFilmService } from '../shared/aboutFilm.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  films!: Film[];
  @Input() aboutFilm!: AboutFilm;
  @Input() aboutFilms!: AboutFilm[];



  constructor(private filmService: FilmService, private aboutFilmService: AboutFilmService) {}

  ngOnInit(): void {
    this.films = this.filmService.getFilms();
    this.aboutFilms = this.aboutFilmService.getAbout();
  }





}
