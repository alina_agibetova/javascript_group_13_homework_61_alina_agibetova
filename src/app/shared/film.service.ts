import { Film } from './film.model';

export class FilmService {
  private films: Film[] = [
    new Film('Дом Gucci', 'https://cinematica.kg/uploads/movies/69ad63d7-ff21-41a0-903e-db42f6696274.jpg/1024/800'),
    new Film('Бумеранг', 'https://cinematica.kg/uploads/movies/df61ec86-251f-4bd6-b3ee-b4d4ed55b631.jpeg/1024/800'),
    new Film('Летчик', 'https://cinematica.kg/uploads/movies/7c048e3e-4943-4891-9816-3cf894e7d098.jpg/1024/800'),
    new Film('Обитель зла: Раккун-Сити', 'https://cinematica.kg/uploads/movies/57469982-9516-40db-b977-2aef33b3e18b.jpg/1024/800'),
    new Film('Энканто ','https://cinematica.kg/uploads/movies/c4084f12-5a1f-449e-a36f-5d36ccc39d9e.jpg/1024/800'),
    new Film('Охотники за привидениями: наследники', 'https://cinematica.kg/uploads/movies/c0333d5f-ff0d-4547-8748-e5768547bf7f.jpg/1024/800'),
    new Film('Король Ричард', 'https://cinematica.kg/uploads/movies/d94b1a65-053c-4aeb-9bd0-cb232d8d7f19.jpg/1024/800'),
  ];

  getFilms(){
    return this.films.slice();
  }

}
