export class AboutFilm {
  constructor(
    public name: string,
    public year: number,
    public director: string,
    public genre: string,
    public country: string,
    public description: string,
  ){}
}
