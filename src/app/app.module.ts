import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import { FilmsComponent } from './films/films.component';
import { ModalComponent } from './ui/modal/modal.component';
import { FilmItemComponent } from './films/film-item/film-item.component';
import { AboutFilmsComponent } from './aboutFilms/aboutFilms.component';
import { AboutFilmItemComponent } from './aboutFilms/aboutFilm-item/aboutFilm-item.component';
import { FilmService } from './shared/film.service';
import { AboutFilmService } from './shared/aboutFilm.service';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactItemComponent } from './contacts/contact-item/contact-item.component';
import { RouterModule, Routes } from '@angular/router';
import { ContactService } from './shared/contact.service';

const routes: Routes = [
  {path: 'films', component: FilmsComponent},
  {path: 'films/aboutFilms', component: AboutFilmsComponent},
  {path: 'films/aboutFilms/contacts', component: ContactsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    FilmsComponent,
    ModalComponent,
    FilmItemComponent,
    AboutFilmsComponent,
    AboutFilmItemComponent,
    ContactsComponent,
    ContactItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [FilmService, AboutFilmService, ContactService],
  bootstrap: [AppComponent]
})
export class AppModule { }
