import { Component, Input, OnInit } from '@angular/core';
import { AboutFilm } from '../shared/aboutFilm.model';
import { AboutFilmService } from '../shared/aboutFilm.service';

@Component({
  selector: 'app-aboutFilms',
  templateUrl: './aboutFilms.component.html',
  styleUrls: ['./aboutFilms.component.css']
})
export class AboutFilmsComponent implements OnInit {
  @Input() aboutFilms!: AboutFilm[];


  constructor(private aboutFilmService: AboutFilmService) {}

  ngOnInit(): void {
    this.aboutFilms = this.aboutFilmService.getAbout();
  }
}
