import { Component, Input } from '@angular/core';
import { AboutFilm } from '../../shared/aboutFilm.model';

@Component({
  selector: 'app-aboutFilm-item',
  templateUrl: './aboutFilm-item.component.html',
  styleUrls: ['./aboutFilm-item.component.css']
})
export class AboutFilmItemComponent {
  @Input() aboutFilm!: AboutFilm;

}
